export interface PostItemInterface {
  id: string
  image: string
  likes: number
  tags: Array<string>
  text: string
  publishDate: string
  owner: {
    id: string
    title: string
    firstName: string
    lastName: string
    picture: string
  }
}
