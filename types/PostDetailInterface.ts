export interface PostDetailInterface {
  id: string;
  image: string;
  likes: number;
  link: string;
  owner: {
    firstName: string;
    id: string;
    lastName: string;
    picture: string;
    title: string;
  };
  publishDate: string;
  tags: Array<string>;
  text: string;
}
