import React from 'react';
import CardDetail from '../components/CardDetail';
import OneColumnLayout from '../components/OneColumnLayout';
import { PostDetailInterface } from '../types/PostDetailInterface';

const API_URL = process.env.REACT_APP_BASE_URL;

const CardDetailPage = ({
  initDetailPost,
}: {
  initDetailPost: PostDetailInterface;
}) => {
  return (
    <OneColumnLayout>
      <CardDetail dataDetail={initDetailPost} />
    </OneColumnLayout>
  );
};

export default CardDetailPage;

export async function getServerSideProps(context: any) {
  const id = context.query.id;

  let jsonDetailPost = {};

  try {
    const [detailPost] = await Promise.all([
      fetch(`${API_URL}/post/${id}`, {
        method: 'GET',
        headers: {
          ['app-id']: '62ada760958cfd2329286eac',
        },
      }),
    ]);

    [jsonDetailPost] = await Promise.all([detailPost.json()]);
  } catch (error) {
    console.log(error);
  }

  return {
    props: {
      initDetailPost: jsonDetailPost,
    },
  };
}
