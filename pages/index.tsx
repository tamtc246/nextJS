import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import styles from '../styles/Home.module.css';
import useSWRInfinite from 'swr/infinite';
import CardItem from '../components/CardItem';
import getConfig from 'next/config';
import CardList from '../components/CardList';
import { useState } from 'react';
import { Container, Pagination } from '@mui/material';
import SyncLoader from 'react-spinners/SyncLoader';
import OneColumnLayout from '../components/OneColumnLayout';

export interface PostsProps {
  initPosts: {
    data: Array<any>;
    limit: number;
    page: number;
    total: number;
  };
}

const API_URL = process.env.REACT_APP_BASE_URL;

const Home = ({ initPosts }: PostsProps) => {
  const [page, setPage] = useState(0);

  const [isLoading, setIsLoading] = useState(false);

  const getKey = (pageIndex: number, previousPageData: any) => {
    if (previousPageData && !previousPageData.length) return null; // reached the end

    return API_URL && `${API_URL}/post?limit=16&page=${page}`; // SWR key
  };

  async function fetcher(url: string) {
    setIsLoading(true);
    const res = await fetch(url, {
      method: 'GET',
      headers: {
        ['app-id']: '62ada760958cfd2329286eac',
      },
    });
    const json = await res.json();
    setIsLoading(false);
    return json;
  }

  const { data, error, mutate, size, setSize, isValidating } = useSWRInfinite(
    getKey,
    fetcher
  );

  const posts = data
    ? [].concat(...data?.map((dt) => dt.data))
    : initPosts.data;

  return (
    <div>
      <Head>
        <title>My Posts</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <OneColumnLayout>
        <Container>
          {isLoading ? (
            <SyncLoader
              color="#000"
              loading={isLoading}
              size={24}
              css="display:flex; justify-content:center;height:100vh; align-items:center;"
            />
          ) : (
            <CardList posts={posts} />
          )}
          {!isLoading && (
            <Pagination
              sx={{
                margin: '60px auto',
                display: 'flex',
                justifyContent: 'center',
              }}
              size="large"
              page={page + 1}
              count={Math.ceil(initPosts.total / initPosts.limit)}
              variant="outlined"
              onChange={(e, page) => setPage(page - 1)}
              color="secondary"
            />
          )}
        </Container>
      </OneColumnLayout>
    </div>
  );
};

export default Home;

export async function getServerSideProps() {
  let jsonPosts = {
    data: [],
    limit: 16,
    page: 0,
    total: 0,
  };

  try {
    const [posts] = await Promise.all([
      fetch(`${API_URL}/post?limit=16&page=0`, {
        method: 'GET',
        headers: {
          ['app-id']: '62ada760958cfd2329286eac',
        },
      }),
    ]);

    [jsonPosts] = await Promise.all([posts.json()]);
  } catch (error) {
    console.log(error);
  }
  return {
    props: {
      initPosts: {
        data: jsonPosts.data,
        limit: jsonPosts.limit,
        page: jsonPosts.page,
        total: jsonPosts.total,
      },
    },
  };
}
