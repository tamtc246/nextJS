import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { ThemeProvider } from '@mui/material';
import DefaultTheme from '../themes/defaultTheme';
import { useEffect, useState } from 'react';

function MyApp({ Component, pageProps }: AppProps) {
  const [showChild, setShowChild] = useState(false);
  useEffect(() => {
    setShowChild(true);
  }, []);

  if (!showChild) {
    return null;
  }

  if (typeof window === 'undefined') {
    return <></>;
  } else
    return (
      <ThemeProvider theme={DefaultTheme}>
        <Component {...pageProps} />
      </ThemeProvider>
    );
}

export default MyApp;
