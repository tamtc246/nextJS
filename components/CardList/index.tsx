import { Box, Grid } from '@mui/material';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { PostItemInterface } from '../../types/PostItemInterface';
import CardItem from '../CardItem';
import SortData, { SORT_DATA } from '../SortData';

const CardList = ({ posts }: { posts: Array<PostItemInterface> }) => {
  const [typeSort, setTypeSort] = useState(SORT_DATA.DEFAULT);

  const [data, setData] = useState<Array<PostItemInterface>>([...posts]);

  const sortData = (type: number) => {
    switch (type) {
      case SORT_DATA.DEFAULT:
        setData([...posts]);
        break;

      case SORT_DATA.LOWEST_LIKES:
        setData([...data?.sort((a, b) => a.likes - b.likes)]);
        break;

      case SORT_DATA.HIGHEST_LIKES:
        setData([...data?.sort((a, b) => b.likes - a.likes)]);
        break;

      case SORT_DATA.NEWEST:
        setData([
          ...data?.sort(
            (a, b) =>
              moment(b.publishDate).valueOf() - moment(a.publishDate).valueOf()
          ),
        ]);
        break;

      case SORT_DATA.OLDEST:
        setData([
          ...data?.sort(
            (a, b) =>
              moment(a.publishDate).valueOf() - moment(b.publishDate).valueOf()
          ),
        ]);
        break;
    }
  };

  return (
    <Box>
      <SortData
        onChange={(e: number) => {
          sortData(e);
        }}
      />
      <Grid container spacing={3}>
        {data?.map((item, idx) => (
          <Grid item key={idx} display="flex">
            <CardItem item={item} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default CardList;
