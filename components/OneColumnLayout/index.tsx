import { Box } from '@mui/material';
import React, { ReactNode } from 'react';
import Footer from '../Footer';
import Header from '../Header';

const OneColumnLayout = ({ children }: { children: ReactNode }) => {
  return (
    <>
      <Header />
      <Box sx={{ margin: '120px auto' }}>{children}</Box>
      <Footer />
    </>
  );
};

export default OneColumnLayout;
