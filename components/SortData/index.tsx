import { Box, FormControl, MenuItem, Select } from '@mui/material';
import React, { useState } from 'react';

export enum SORT_DATA {
  DEFAULT = -1,
  HIGHEST_LIKES,
  LOWEST_LIKES,
  NEWEST,
  OLDEST,
}

const SortData = ({ onChange }: { onChange: Function }) => {
  const [type, setType] = useState(-1);

  const DATA_SORT = [
    {
      label: 'Default',
      value: SORT_DATA.DEFAULT,
    },
    {
      label: 'Hightest likes',
      value: SORT_DATA.HIGHEST_LIKES,
    },
    {
      label: 'Lowest likes',
      value: SORT_DATA.LOWEST_LIKES,
    },
    {
      label: 'Newest',
      value: SORT_DATA.NEWEST,
    },
    {
      label: 'Oldest',
      value: SORT_DATA.OLDEST,
    },
  ];

  const handleChange = (e: any) => {
    setType(e.target.value);
    onChange(e.target.value);
  };

  return (
    <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
      <FormControl
        sx={{
          m: 1,
          minWidth: 240,
          marginBottom: '40px',
          '& .MuiOutlinedInput-root': {
            borderRadius: '16px',
          },
        }}
      >
        <Select
          value={type}
          onChange={handleChange}
          displayEmpty
          inputProps={{ 'aria-label': 'Without label' }}
        >
          {DATA_SORT.map((item, idx) => (
            <MenuItem value={item.value} key={idx} sx={{ fontWeight: 700 }}>
              {item.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};

export default SortData;
