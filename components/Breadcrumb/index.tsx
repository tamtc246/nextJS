import { Breadcrumbs, Typography } from '@mui/material';
import React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import GrainIcon from '@mui/icons-material/Grain';
import Link from 'next/link';
const Breadcrumb = ({ title }: { title?: string }) => {
  return (
    <Breadcrumbs
      aria-label="breadcrumb"
      sx={{
        marginBottom: '20px',
        ['li, p']: {
          color: (theme) => theme.palette.black.main,
          fontSize: '24px',
          fontWeight: 'bold',
        },
        a: {
          display: 'flex',
          alignItems: 'center',
        },
      }}
    >
      <Link href="/" passHref>
        <a>
          <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          HOME
        </a>
      </Link>
      <Typography sx={{ display: 'flex', alignItems: 'center' }}>
        {title}
      </Typography>
    </Breadcrumbs>
  );
};

export default Breadcrumb;
