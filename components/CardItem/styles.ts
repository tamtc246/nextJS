import { Box, Typography } from '@mui/material';
import { styled } from '@mui/system';

export const StyledCardItem = styled(Box)(({ theme }) => ({
  background: theme.palette.white.main,
  boxShadow: '1px 2px 12px 8px #00000029',
  padding: 0,
  borderRadius: '16px',
  width: '300px',
  transform: 'translateY(0)',
  transition: '0.4s',
  cursor: 'pointer',
  boxSizing: 'border-box',
  '&:hover': {
    transform: 'translateY(-16px)',
  },
}));

export const StyledCardContent = styled(Box)(({ theme }) => ({
  padding: '8px',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
}));

export const StyledCardImage = styled(Box)<{ image: string }>(
  ({ theme, image }) => ({
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100%',
    height: '262px',
    borderRadius: '16px',
    position: 'relative',
  })
);

export const StyledHeart = styled(Box)(({ theme }) => ({
  borderRadius: '4px',
  padding: '8px',
  fontWeight: 700,
  fontSize: '14px',
  lineHeight: '15px',
  letterSpacing: '0.04em',
  position: 'absolute',
  right: '20px',
  bottom: '20px',
  color: theme.palette.white.main,
  background: 'rgba(0, 0, 0, 0.3)',
  display: 'flex',
  alignItems: 'center',
  img: {
    margin: 'unset !important',
    marginRight: '10px !important',
  },
}));

export const StyledCardTitle = styled(Typography)(({ theme }) => ({
  color: theme.palette.black.main,
  fontWeight: 700,
  fontSize: '18px',
  display: '-webkit-box',
  '-webkit-line-clamp': '2',
  '-webkit-box-orient': 'vertical',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  wordBreak: 'break-word',
  minHeight: '60px',
  paddingTop: '8px',
  textTransform: 'capitalize',
}));

export const StyledCardName = styled(Typography)(({ theme }) => ({
  color: theme.palette.black.main,
  fontWeight: 'bold',
  fontSize: '16px',
  paddingTop: '8px',
  textTransform: 'capitalize',
}));

export const StyledAvatar = styled('img')(({ theme }) => ({
  borderRadius: '50%',
  height: '40px',
  width: '40px',
  marginRight: '10px',
  boxShadow: '0px 0px 8px 3px #00000087',
}));

export const StyledTag = styled(Box)(({ theme }) => ({
  color: theme.palette.white.main,
  fontWeight: 700,
  fontSize: '18px',
  lineHeight: '26px',
  background: theme.palette.error.main,
  marginRight: '12px',
  marginBottom: '12px',
  padding: '2px 10px',
  borderRadius: '8px',
  boxShadow: ' 0px 0px 3px 3px #00000087',
}));

export const StyledDate = styled(Box)(({ theme }) => ({
  color: theme.palette.neutral.gray4,
  fontWeight: 400,
  fontSize: '14px',
}));
