import React from 'react';
import { PostItemInterface } from '../../types/PostItemInterface';
import {
  StyledAvatar,
  StyledCardContent,
  StyledCardImage,
  StyledCardItem,
  StyledCardName,
  StyledCardTitle,
  StyledDate,
  StyledHeart,
  StyledTag,
} from './styles';
import IconHeart from '../../assets/heart.svg';
import Image from 'next/image';
import moment from 'moment';
import { Box } from '@mui/material';
import { useRouter } from 'next/router';
const CardItem = ({ item }: { item: PostItemInterface }) => {
  const router = useRouter();

  return (
    <StyledCardItem onClick={() => router.push(`/${item?.id}`)}>
      <StyledCardContent>
        <StyledCardImage image={item?.image}>
          <StyledHeart>
            <Image src={IconHeart.src} alt="heart" width={20} height={20} />
            {item?.likes}
          </StyledHeart>
        </StyledCardImage>
        <Box sx={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: '10px',
            }}
          >
            <StyledCardName>
              {item?.owner?.title}.{' '}
              {item?.owner?.firstName + ' ' + item?.owner?.lastName}
            </StyledCardName>
            <StyledAvatar src={item?.owner?.picture} alt="avatar" />
          </Box>
          <StyledCardTitle>{item?.text}</StyledCardTitle>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'start',
              flexWrap: 'wrap',
              marginTop: '8px',
            }}
          >
            {item?.tags?.map((o, idx) => (
              <StyledTag key={idx}>{o}</StyledTag>
            ))}
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              paddingTop: '10px',
              marginTop: 'auto',
            }}
          >
            <StyledDate>{moment(item?.publishDate).format('hh:mm')}</StyledDate>
            <StyledDate>
              {moment(item?.publishDate).format('DD/MM/YYYY')}
            </StyledDate>
          </Box>
        </Box>
      </StyledCardContent>
    </StyledCardItem>
  );
};

export default CardItem;
