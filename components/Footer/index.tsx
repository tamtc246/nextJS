/* eslint-disable @next/next/no-img-element */
import { Box, Container, Typography } from '@mui/material';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import Logo from '/public/vercel.svg';

const Footer = () => {
  const router = useRouter();

  return (
    <Box
      sx={{
        height: '60px',
        background: (theme) => theme.palette.error.main,
        position: 'fixed',
        width: '100%',
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
      }}
    >
      <Container>
        <Box
          sx={{
            padding: '20px 0 !important',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: '60px',
            img: {
              cursor: 'pointer',
              height: '30px',
            },
          }}
        >
          <img src={Logo.src} alt="" onClick={() => router.push('/')} />
          <Typography
            sx={{
              color: (theme) => theme.palette.white.main,
              fontSize: '20px',
              fontWeight: 'bold',
            }}
          >
            Tran Tam 246
          </Typography>
        </Box>
      </Container>
    </Box>
  );
};

export default Footer;
