import { Box, Typography } from '@mui/material';
import { styled } from '@mui/system';

export const StyledImage = styled(Box)<{ image?: string }>(
  ({ theme, image }) => ({
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: '100%',
    height: '75vh',
    borderRadius: '16px',
    position: 'relative',
    transition: '0.4s',
    overflow: 'hidden',
    '&:hover .content': {
      // animation: 'myEffect 0.8s',
      opacity: 1,
      transform: 'translateX(0)',
    },
  })
);

export const StyledContent = styled(Box)(({ theme }) => ({
  position: 'absolute',
  width: '50%',
  inset: 0,
  background: 'rgba(255,255,255, 0.6)',
  opacity: 0,
  transform: 'translateX(-50%)',
  padding: '20px 30px',
  transition: '0.4s',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
}));

export const StyledLabel = styled(Typography)(({ theme }) => ({
  color: theme.palette.black.main,
  fontSize: '18px',
  fontWeight: 600,
  marginRight: '20px',
  flex: 1,
}));

export const StyledValue = styled(Typography)(({ theme }) => ({
  color: theme.palette.black.main,
  fontSize: '18px',
  fontWeight: 600,
  display: 'flex',
  alignItems: 'center',
  flex: 5,
  textTransform: 'capitalize',
}));
