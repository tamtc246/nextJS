import { Box, Container } from '@mui/material';
import moment from 'moment';
import React from 'react';
import { PostDetailInterface } from '../../types/PostDetailInterface';
import Breadcrumb from '../Breadcrumb';
import { StyledAvatar, StyledDate, StyledTag } from '../CardItem/styles';
import { StyledContent, StyledImage, StyledLabel, StyledValue } from './styles';

const CardDetail = ({ dataDetail }: { dataDetail?: PostDetailInterface }) => {
  const detail = [
    {
      label: 'Author:',
      value: (
        <>
          {dataDetail?.owner?.title +
            '. ' +
            dataDetail?.owner?.firstName +
            ' ' +
            dataDetail?.owner?.lastName}
          <a href={dataDetail?.link} target="_blank" rel="noreferrer">
            <StyledAvatar
              src={dataDetail?.owner?.picture}
              alt="avatar"
              sx={{ marginLeft: '20px' }}
            />
          </a>
        </>
      ),
    },
    {
      label: 'Likes:',
      value: dataDetail?.likes,
    },
    {
      label: 'Tags: ',
      value: (
        <Box sx={{ display: 'flex' }}>
          {dataDetail?.tags?.map((o, idx) => (
            <StyledTag key={idx}>{o}</StyledTag>
          ))}
        </Box>
      ),
    },
  ];

  return (
    <Container>
      <Breadcrumb title={dataDetail?.text} />
      <Box sx={{ display: 'flex' }}>
        <StyledImage image={dataDetail?.image}>
          <StyledContent className="content">
            <StyledDate
              sx={{ color: (theme: any) => theme.palette.neutral.gray10 }}
            >
              {moment(dataDetail?.publishDate).format('hh:mm DD/MM/YYYY')}
            </StyledDate>

            {detail?.map((item, idx) => (
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  marginBottom: '20px',
                }}
                key={idx}
              >
                <StyledLabel>{item?.label}</StyledLabel>
                <StyledValue>{item?.value}</StyledValue>
              </Box>
            ))}
          </StyledContent>
        </StyledImage>
      </Box>
    </Container>
  );
};

export default CardDetail;
