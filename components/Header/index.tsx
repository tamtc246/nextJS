import { Box, Container } from '@mui/material';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import Logo from '/public/vercel.svg';
const Header = () => {
  const router = useRouter();

  return (
    <Box
      sx={{
        height: '80px',
        background: (theme) => theme.palette.error.main,
        position: 'fixed',
        width: '100%',
        inset: 0,
        zIndex: 9999,
      }}
    >
      <Container sx={{ height: '100%', display: 'flex', alignItems: 'center' }}>
        <Box
          sx={{
            img: {
              cursor: 'pointer',
            },
          }}
        >
          <Image
            src={Logo.src}
            alt=""
            style={{ color: 'white' }}
            width={200}
            height={60}
            onClick={() => router.push('/')}
          />
        </Box>
      </Container>
    </Box>
  );
};

export default Header;
