import { createTheme } from '@mui/material/styles';
import { SimplePaletteColorOptions } from '@mui/material/styles/createPalette';

const DefaultTheme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: '#00E5FF',
      contrastText: '#FFFFFF',
    },
    secondary: {
      main: '#51BF29',
    },
    error: {
      main: '#F04F5B',
    },
    success: {
      main: '#28CE7E',
    },
    black: {
      main: '#202225',
      10: '#303339',
      20: '#41444B',
    },
    white: {
      main: '#FFFFFF',
      10: '#D1D1D1',
      20: '#B8B8B8',
    },
    neutral: {
      main: '#171A23',
      gray1: '#707A8A',
      gray2: '#E0E0E0',
      gray3: '#A2A3A7',
      gray4: '#868686',
      gray5: '#A4A5A7',
      gray6: '#9D9D9D',
      gray7: '#535353',
      gray8: '#D8D8D8',
      gray9: '#6A6A6A',
      gray10: '#262A31',
      gray11: '#3D3D3D',
      gray12: 'rgba(164, 163, 163, 0.5)',
      gray13: 'rgba(255, 255, 255, 0.6)',
      gray14: '#C1C1C1',
      gray15: '#ABABAB',
      gray16: '#C5C5C5',
      gray17: '#C4C4C4',
      gray18: 'rgba(48, 51, 57, 0.7)',
      gray19: '#707070',
      gray20: '#D9D9D9',
      gray21: '#798390',
      gray22: '#3F4144',
      black1: 'rgba(0,0,0,0.8)',
      black2: 'rgba(0, 0, 0, 0.32)',
      black3: 'rgba(0, 0, 0, 0.6)',
      black4: '#000000',
      black5: '#454954',
      black6: '#676C7C',
      black7: 'rgba(0, 0, 0, 0.7)',
      black8: '#201c1c66',
      black9: 'rgba(31, 34, 41, 0.8)',
      black10: 'rgba(29, 31, 35, 0.85)',
      black11: '#353840',
      black12: 'rgba(29, 31, 35, 0.8)',
      black13: 'rgba(0, 0, 0, 0.9)',
      red1: '#DC401E',
      blue1: '#02C4DA',
      green1: '#59D645',
      green2: '#72EC26',
      green3: '#66D13E',
      cyan1: '#41ECD7',
      purple1: '#C957FF',
      gold1: '#FFBF4D',
      black0: 'rgba(0,0,0)',
    },
  },

  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1328,
      xl: 1328,
    },
  },
});

interface Shade extends SimplePaletteColorOptions {
  10: string;
  20: string;
}

interface Neutral {
  [key: string]: string;
}

interface ThemeCustomColors {
  black?: true;
}

declare module '@mui/material/styles' {
  interface Palette {
    black: Palette['primary'];
    white: Palette['primary'];
    neutral: Neutral;
  }

  // allow configuration using `createTheme`
  interface PaletteOptions {
    black?: Shade;
    white?: Shade;
    neutral?: Neutral;
  }
}

// Update the Button's color prop options
declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides extends ThemeCustomColors {}
}
declare module '@mui/material/TextField' {
  interface TextFieldPropsColorOverrides extends ThemeCustomColors {}
}
declare module '@mui/material/Checkbox' {
  interface CheckboxPropsColorOverrides extends ThemeCustomColors {}
}
declare module '@mui/material/Radio' {
  interface RadioPropsColorOverrides extends ThemeCustomColors {}
}
declare module '@mui/material/Switch' {
  interface SwitchPropsColorOverrides extends ThemeCustomColors {}
}

export default DefaultTheme;
